FROM maven:3.9.6-eclipse-temurin-21 AS build
WORKDIR /workspace/app
COPY pom.xml .
RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline
COPY . .
RUN mvn clean package -Dmaven.test.skip=true

FROM eclipse-temurin:21.0.2_13-jre
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build /workspace/app/target/tracker.jar app.jar
RUN addgroup app
RUN useradd -g app app
USER app
EXPOSE 9000
ENTRYPOINT ["java", "-jar", "app.jar"]
