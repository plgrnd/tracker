package com.example;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "visit")
public class Visit {
    @Id
    private String id;

    private String ip;

    private String app;

    private Date timestamp;

    public Visit() {
        setTimestamp(new Date());
    }

    public Visit(String ip) {
        this();
        setIp(ip);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    private void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "ip='" + ip + '\'' +
                ", app='" + app + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
