package com.example;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class VisitController {

    public static final Pattern ALPHANUMERIC = Pattern.compile("^[a-zA-Z0-9]+$");
    private final VisitRepository repository;

    VisitController(VisitRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/")
    public ResponseEntity<Visit> create(HttpServletRequest request) {
        Visit visit = new Visit(request.getRemoteAddr());

        String app = request.getHeader("APP");
        if (null != app) {
            Matcher matcher = ALPHANUMERIC.matcher(app);
            if (!matcher.matches()) {
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            visit.setApp(app);
        }

        return new ResponseEntity<>(repository.save(visit), HttpStatus.OK);
    }
}
