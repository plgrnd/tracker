# Tracker
A legacy Spring Boot project that logs the visits, candidate for refactoring.

- Spring Boot 3
- Java 21

```plantuml
@startuml
!include <C4/C4_Component>
title C4 Container Diagram
left to right direction
Person_Ext(person_visitor, "Visitor", "submits visits")
System(system_tracker, "Tracker", "")
System_Boundary(system_tracker, "") {
    Component(component_app, "REST App", "Spring Boot", "handles visits")
    note top of component_app
        app:9000
    end note
    ComponentDb(component_db, "NoSQL", "MongoDB", "persists visits")
    note top of component_db
        db:27017
    end note
    Rel(component_app, component_db, "Wire", "TCP")
}
Rel(person_visitor, component_app, "browser", "HTTP")
@enduml
```

```plantuml
title Data Model
class Visit {
   -id : String
   +ip : String
   +timestamp : Date
   +app : String
}
```

## Development

- start `db` service in `docker-compose.yaml`
- in the first terminal, run the application
  ```shell
  MONGO_DB=tracker MONGO_HOST=localhost mvn spring-boot:run
  ```
- in the second terminal, check the API
  ```shell
  curl -X POST localhost:9000
  # {"ip":"127.0.0.1","app":null,"timestamp":"2024-08-27T09:54:18.673+00:00"}
  curl -X POST -H 'APP:test' localhost:9000
  # {"ip":"127.0.0.1","app":"test","timestamp":"2024-08-27T09:54:46.723+00:00"}
  ```

## Refactoring Objectives

1. Alignment with Domain-Driven Design principles.
2. Comprehensive test coverage.
